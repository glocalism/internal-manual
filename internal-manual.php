<?php
/*
Plugin Name: Internal Manual
Plugin URI: 
Description: Create undisclosed manual for internal use 
Author: Yusuke Kawato
Version: 0.2
Author URI: http://glocalism.co.jp/
Bitbucket Plugin URI: https://bitbucket.org/glocalism/internal-manual/
License: GPL2
*/

/*  Copyright 2016 Yusuke Kawato

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

if ( !class_exists('InternalManual') ):

define( 'IM_PATH', plugin_dir_path(__FILE__) );
load_plugin_textdomain('internal-manual', false, basename( dirname( __FILE__ ) ) . '/languages' );

class InternalManual
{
    function __construct( $options = array() ) 
    {
        add_action( 'activate_plugin', array( $this, 'activate_plugin' ) );
        add_action( 'init', array( $this, 'init' ) );
        add_action( 'template_redirect', array( $this, 'template_redirect' ) );

        // 


        add_action('admin_menu', array( $this, 'im_add_page' ) );

        add_action('admin_init', array( $this, 'plugin_admin_init' ) );

    }




    function plugin_admin_init(){
        register_setting( 'im_plugin_options', 'im_plugin_options', array( $this, 'plugin_options_validate' ) );
        add_settings_section('plugin_main', 'Main Settings', array( $this, 'plugin_section_text') , 'plugin');

        add_settings_field('internal-manual-editable', '編集権限', array( $this, 'plugin_editable_role' ), 'plugin', 'plugin_main');
        add_settings_field('internal-manual-viewable', '閲覧権限', array( $this, 'plugin_viewable_role' ), 'plugin', 'plugin_main');

    }

    function plugin_options_validate($input) {
        $newinput = $input;
    
        // editable_roles
        // remove from all roles
        $roles = get_editable_roles();

        foreach( $roles as $role_name =>  $role_info ){
            if( $role_name == 'administrator' )
                continue;

            $role = get_role( $role_name );

            $role->remove_cap( 'edit_internal_manual' ); 
            $role->remove_cap( 'read_internal_manual' ); 
            $role->remove_cap( 'delete_internal_manual' ); 
            $role->remove_cap( 'edit_others_internal_manual' ); 
            $role->remove_cap( 'delete_internal_manual' ); 
            $role->remove_cap( 'publish_internal_manual' ); 
            $role->remove_cap( 'read_private_internal_manual' ); 

        }
    
    
    //     capability_type add these to each 'editable_roles' role
        if( !empty( $input['editable_roles'] ) ){
            foreach( $input['editable_roles'] as $role_name => $on ){
                // 
                $role = get_role( $role_name );

                $role->add_cap( 'edit_internal_manual' ); 
                $role->add_cap( 'read_internal_manual' ); 
                $role->add_cap( 'delete_internal_manual' ); 
                $role->add_cap( 'edit_others_internal_manual' ); 
                $role->add_cap( 'delete_internal_manual' ); 
                $role->add_cap( 'publish_internal_manual' ); 
                $role->add_cap( 'read_private_internal_manual' ); 

            }
        }
        return $newinput;
    }

    function plugin_section_text() {
        echo '<p>Internal Manual Plugin settings here.</p>';
    }


    // 編集権限
    function plugin_editable_role() {
        $options = get_option('im_plugin_options');

        $roles = get_editable_roles();
        $keys = array_keys( $roles );

        $editable_roles = @array_keys( $options['editable_roles'] );
        foreach( $roles as $role_name =>  $role_info ){

            if( $role_name == 'administrator' )
                continue;

            $checked = '';
            if( @in_array( $role_name, $editable_roles ) )
                $checked = 'checked="checked"';
            echo '<div><label><input type="checkbox" name="im_plugin_options[editable_roles][' . $role_name . '] value="' . $role_name . '" ' . $checked . '>' . $role_info['name'] . '</label></div>';
        }

    } 

    // 閲覧権限
    function plugin_viewable_role() {
        $options = get_option('im_plugin_options');

        $roles = get_editable_roles();
        $keys = array_keys( $roles );

        $viewable_roles = @array_keys( $options['viewable_roles'] );
        foreach( $roles as $role_name =>  $role_info ){

            if( $role_name == 'administrator' )
                continue;

            $checked = '';
            if( @in_array( $role_name, $viewable_roles ) )
                $checked = 'checked="checked"';
            echo '<div><label><input type="checkbox" name="im_plugin_options[viewable_roles][' . $role_name . '] value="' . $role_name . '" ' . $checked . '>' . $role_info['name'] . '</label></div>';
        }
        // any
        $checked= '';
        if( @in_array( 'any', $viewable_roles ) )
            $checked = 'checked="checked"';
            
        echo '<div><label><input type="checkbox" name="im_plugin_options[viewable_roles][any] value="any" ' . $checked . '>' . __('Any', 'internal-manual') . '</label></div>';
    
    } 






    function im_add_page() {
        add_submenu_page(
            'edit.php?post_type=internal_manual',
            __( 'Internal Manual Settings', 'internal-manual' ),
            __( 'Settings' ),
            'manage_options',
            'internal-manual-settings', // 'books-shortcode-ref',
            array( $this, 'submenu_page_callback' )
        );
    }
 


    function submenu_page_callback() { 
        ?>

        <div class="wrap">
            <h1><?php _e( 'Internal Manual Settings', 'internal-manual' ); ?></h1>
            Options relating to the Custom Plugin.

            <form action="options.php" method="post">

                <?php settings_fields('im_plugin_options'); ?>
                <?php do_settings_sections('plugin'); ?>
 
                <input name="Submit" type="submit" value="<?php esc_attr_e('Save Changes'); ?>" />
            </form>
        </div>

        <?php
    }




    function activate_plugin(){
        // add roles to admin
        $role = get_role( 'administrator' );

        $role->add_cap( 'edit_internal_manual' ); 
        $role->add_cap( 'read_internal_manual' ); 
        $role->add_cap( 'delete_internal_manual' ); 
        $role->add_cap( 'edit_others_internal_manual' ); 
        $role->add_cap( 'delete_internal_manual' ); 
        $role->add_cap( 'publish_internal_manual' ); 
        $role->add_cap( 'read_private_internal_manual' ); 

    }
    
    
    function init()
    {    

//         $role = get_role( 'administrator' );
        // register post type internal_manual



        $labels = array(
            'name'               => __('Internal Manual', 'internal-manual'), 
            'singular_name'      => __('Internal Manual', 'internal-manual'), 
            'menu_name'          => __('Internal Manual', 'internal-manual'), 
            'name_admin_bar'     => __('Internal Manual', 'internal-manual'), 
                    'add_new' => __('Add New', 'internal-manual'), 
                    'add_new_item' => __('Add New Internal Manual', 'internal-manual'), 
                    'new_item' => __('New Internal Manual', 'internal-manual'), 
                    'edit_item' => __('Edit Internal Manual', 'internal-manual'), 
                    'view_item' => __('View Internal Manuals', 'internal-manual'), 
                    'all_items' => __('All Internal Manuals', 'internal-manual'), 
                    'search_items' => __('Search Internal Manuals', 'internal-manual'), 
            'parent_item_colon'  => __( 'Parent Manuals:', 'internal-manual' ),
            'not_found'          => __( 'No manuals found.', 'internal-manual' ),
            'not_found_in_trash' => __( 'No manuals found in Trash.', 'internal-manual' )
        );

        $args = array(
            'labels'             => $labels,
            'description'        => __( 'Internal Manual', 'internal-manual' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'internal_manual' ),
            'capability_type'    => array( 'internal_manual', 'internal_manuals' ), // 'post',
//             'capabilities'       =>  array(
//                 'edit_post'          => 'update_core',
//                 'read_post'          => 'update_core',
//                 'delete_post'        => 'update_core',
//                 'edit_posts'         => 'update_core',
//                 'edit_others_posts'  => 'update_core',
//                 'delete_posts'       => 'update_core',
//                 'publish_posts'      => 'update_core',
//                 'read_private_posts' => 'update_core'
//             ),
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
        );
    	register_post_type( 'internal_manual', $args );

        // create hierarchical taxonomy
        $args = array(
            'hierarchical'      => true,
            'labels'            => array(

                'name'              => __( 'Categories' ),
                'singular_name'     => __( 'Category' ),
                'search_items'      => __( 'Search Categories' ),
                'all_items'         => __( 'All Categories' ),
                'parent_item'       => __( 'Parent Category' ),
                'parent_item_colon' => __( 'Parent Category:' ),
                'edit_item'         => __( 'Edit Category' ),
                'update_item'       => __( 'Update Category' ),
                'add_new_item'      => __( 'Add New Category' ),
                'new_item_name'     => __( 'New Category Name' ),
                'menu_name'         => __( 'Category' ),
            ),
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'manual_cat' ),
        );

        register_taxonomy( 'manual_cat', array( 'internal_manual' ), $args );


        
        
    } // end init()
    


    function template_redirect(){ 
        global $post;

        if( $post->post_type == 'internal_manual' ){


            $options = get_option('im_plugin_options');
            $viewable_roles = $options['viewable_roles'];
//             print_r($viewable_roles);
            if( $viewable_roles['any'] == 'on' )
                return;
            
            // 
            if( is_user_logged_in() ){
                global $current_user;
                $user_role = $current_user->roles[0];
                if( $user_role == 'administrator' )
                    return;
                
                $viewable = array_keys( $viewable_roles );
                if( in_array( $user_role, $viewable ) )
                    return;
                
                // 
                wp_redirect( site_url() );
                exit;
                
            }

        }
        
        
        // archive page
        global $wp_query;
        if( is_archive() ){
            $query_vars = $wp_query->query_vars;
            if( $query_vars['post_type'] == 'internal_manual' ){


                $options = get_option('im_plugin_options');
                $viewable_roles = $options['viewable_roles'];
    //             print_r($viewable_roles);
                if( $viewable_roles['any'] == 'on' )
                    return;
            
                // 
                if( is_user_logged_in() ){
                    global $current_user;
                    $user_role = $current_user->roles[0];
                    if( $user_role == 'administrator' )
                        return;
                
                    $viewable = array_keys( $viewable_roles );
                    if( in_array( $user_role, $viewable ) )
                        return;
                
                    // 
                    wp_redirect( site_url() );
                    exit;
                
                }

            }
        }
    }
    

    // archive pageのcontent
    
    // singleのcontent
    
    
}


$InternalManual = new InternalManual();

endif; // class_exists



?>