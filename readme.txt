=== Internal Manual ===
Contributors: ykawato
Tags: widget, sidebar, php
Requires at least: 3.0
Tested up to: 3.5.1
Stable tag: 0.2

This plugin makes internal manual post type only viewable to users with accessible roles.

== Description ==

= Intro = 


== Installation ==

1. Copy the `internal-manual` directory into your `wp-content/plugins` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Create your manuals in `Internal Manuals` just as you would create posts. Hierarchical taxonomy manual_cat is available.
4. That's it! :)

== Known Issues / Bugs ==

== Frequently Asked Questions ==

== Changelog ==

= 1.1 = 
* added hierarchical taxonomy manual_cat
= 1.0 =
*	Initial release


== Translations ==

*	ja by [Yusuke Kawato](http://glocalism.co.jp/)

== Uninstall ==

1. Deactivate the plugin
2. That's it! :)

