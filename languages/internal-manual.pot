msgid ""
msgstr ""
"Project-Id-Version: internal-manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-01-06 18:15+0900\n"
"PO-Revision-Date: 2017-01-06 18:15+0900\n"
"Last-Translator: Yusuke Kawato <ykawtao@gmail.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _;_e;__\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../internal-manual.php:156
msgid "Any"
msgstr ""

#: ../internal-manual.php:168
#: ../internal-manual.php:182
msgid "Internal Manual Settings"
msgstr ""

#: ../internal-manual.php:169
msgid "Settings"
msgstr ""

#: ../internal-manual.php:224
#: ../internal-manual.php:225
#: ../internal-manual.php:226
#: ../internal-manual.php:227
#: ../internal-manual.php:242
msgid "Internal Manual"
msgstr ""

#: ../internal-manual.php:228
msgid "Add New"
msgstr ""

#: ../internal-manual.php:229
msgid "Add New Internal Manual"
msgstr ""

#: ../internal-manual.php:230
msgid "New Internal Manual"
msgstr ""

#: ../internal-manual.php:231
msgid "Edit Internal Manual"
msgstr ""

#: ../internal-manual.php:232
msgid "View Internal Manuals"
msgstr ""

#: ../internal-manual.php:233
msgid "All Internal Manuals"
msgstr ""

#: ../internal-manual.php:234
msgid "Search Internal Manuals"
msgstr ""

#: ../internal-manual.php:235
msgid "Parent Manuals:"
msgstr ""

#: ../internal-manual.php:236
msgid "No manuals found."
msgstr ""

#: ../internal-manual.php:237
msgid "No manuals found in Trash."
msgstr ""

#: ../internal-manual.php:272
msgid "Categories"
msgstr ""

#: ../internal-manual.php:273
#: ../internal-manual.php:282
msgid "Category"
msgstr ""

#: ../internal-manual.php:274
msgid "Search Categories"
msgstr ""

#: ../internal-manual.php:275
msgid "All Categories"
msgstr ""

#: ../internal-manual.php:276
msgid "Parent Category"
msgstr ""

#: ../internal-manual.php:277
msgid "Parent Category:"
msgstr ""

#: ../internal-manual.php:278
msgid "Edit Category"
msgstr ""

#: ../internal-manual.php:279
msgid "Update Category"
msgstr ""

#: ../internal-manual.php:280
msgid "Add New Category"
msgstr ""

#: ../internal-manual.php:281
msgid "New Category Name"
msgstr ""

